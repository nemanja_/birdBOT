import os

from PySide.QtGui import QDialog, QApplication, QFormLayout, QPushButton, QLineEdit, QDialogButtonBox, QLabel, \
    QFileDialog, QIcon


class GenerateDialog(QDialog):

    def __init__(self):
        super(GenerateDialog, self).__init__()

        self.setWindowTitle("File generator")
        self.setWindowIcon(QIcon("resources/window_icon.png"))
        rect = QApplication.desktop().screenGeometry()
        height = rect.height()
        width = rect.width()
        self.setGeometry(width / 3, height / 3, width / 3, height / 8)

        formLayout = QFormLayout()

        self.dir = QLineEdit()
        self.dir.setReadOnly(True)
        formLayout.addRow(QLabel("Select the excel table"), self.dir)
        browseBtn = QPushButton("Browse")
        browseBtn.clicked.connect(self.browseAction)

        formLayout.addWidget(browseBtn)

        btnOk = QPushButton("Ok")
        btnOk.clicked.connect(self.okAction)

        btnCancel = QPushButton("Cancel")
        btnCancel.clicked.connect(self.reject)

        group = QDialogButtonBox()
        group.addButton(btnOk, QDialogButtonBox.AcceptRole)
        group.addButton(btnCancel, QDialogButtonBox.RejectRole)
        formLayout.addRow(group)

        self.setLayout(formLayout)
        self.setWindowIcon(QIcon("resources/window_icon.png"))
        self.__result = None

    def browseAction(self):
        name = QFileDialog.getOpenFileName(self, 'Open file', os.getcwd(),"Excel file (*xlsx)")
        self.dir.setReadOnly(False)
        self.dir.setText(name[0])
        self.dir.setReadOnly(True)

    def okAction(self):
        self.__result = self.dir.text()
        self.accept()

    def result(self):
        return self.__result
