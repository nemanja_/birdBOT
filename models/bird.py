# -*- coding: utf-8 -*-

class Bird:

    __slots__ = ['order', 'family', 'genus', 'species_sci', 'species_eng']

    def __init__(self, order, family, genus, species_sci, species_eng):
        self.order       = order
        self.family      = family
        self.genus       = genus
        self.species_sci = species_sci
        self.species_eng = species_eng

    def __str__(self):
        return "ORDER : " + self.order.encode('utf-8') + "\n" + \
               "FAMILY : " + self.family.encode('utf-8')  + "\n" + \
               "GENUS : " + self.genus.encode('utf-8')  + "\n" + \
               "SCIENTIFIC NAME : " + self.species_sci.encode('utf-8')  + "\n" + \
               "ENGLISH NAME : " + self.species_eng.encode('utf-8')