import sys

from PySide.QtGui import QApplication, QMessageBox, QIcon
from pickle import load

from actions.generate_action import generateFile
from gui.mainwindow import MainWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.webPreview = None

    try:
        app.bird_index = load(open('data/bird-index.avian', 'rb'))
    except IOError:
        alpha = QMessageBox()
        alpha.setWindowIcon(QIcon("resources/window_icon.png"))
        alpha.setWindowTitle("Warning")
        alpha.setText("File not found. Generating!")
        alpha.addButton(QMessageBox.Ok)
        alpha.exec_()
        generateFile()
        app.bird_index = load(open('data/bird-index.avian', 'rb'))
    except EOFError:
        alpha = QMessageBox()
        alpha.setWindowIcon(QIcon("resources/window_icon.png"))
        alpha.setWindowTitle("Error")
        alpha.setText("Data file corrupted. Regenerating!")
        alpha.addButton(QMessageBox.Ok)
        alpha.exec_()
        generateFile()
        app.bird_index = load(open('data/bird-index.avian', 'rb'))

    app.mainWindow = MainWindow()
    app.mainWindow.show()
    sys.exit(app.exec_())

