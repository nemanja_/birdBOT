# -*- coding: utf-8 -*-

from urllib2 import *
import re

# THIS SCRIPT WILL ENABLE YOU A TO SEARCH FOR BIRD INFO BY TYPING ITS ENGLISH NAME
# YOU NEED TO HAVE THE FILE IN THE SAME FOLDER AS THE SCRIPT
# YOU MAY NEED TO CHANGE THE FILENAME VARIABLE BELOW IF NEED BE
from PySide.QtGui import QApplication, QMessageBox, QIcon

FILENAME = 'allBirds.avian'


def findAuthor(src):
    match = re.findall('name": (.*?),', src)
    return match[2][1:-1].decode('utf-8')


def findLargestImg(src):
    match = (re.findall('"width":[0-9]+,"height":[0-9]+,"url":(.*?),', src))
    return match[-1][1:-1].replace('\/\/', "http://").replace('\/', '//')


def createPost(data):
    return '<center> <img src = "' + data['LARGEST_IMG'] + '" width = 500 height = 500> </center> <br>' + data['LARGEST_IMG'] + '<br>' + \
           '<b>' + data['BIRD_INFO'].species_eng + '</b>' + " (<i>" + data[
               'BIRD_INFO'].species_sci + "</i>) >>by " + '<a href = "' + data['ORIGINAL_LINK'] + '">' + data[
               'AUTHOR'] + "</a> <br>" + \
           "\n#" + data['BIRD_INFO'].species_eng + " #" + data['BIRD_INFO'].species_sci + " #Aves #" + \
           data['BIRD_INFO'].order.lower().capitalize() + " #" + data['BIRD_INFO'].family


def bird_search(name, link):
    BIRD_DICTIONARY = QApplication.instance().bird_index  # LOADING THE FILE

    if (name == "" or name is None or link == "" or link is None):
        alpha = QMessageBox()
        alpha.setWindowIcon(QIcon("resources/window_icon.png"))
        alpha.setWindowTitle("Warning")
        alpha.setText("You haven't filled in everything!")
        alpha.addButton(QMessageBox.Ok)
        alpha.exec_()
    else:
        POST_DATA = {}
        SUCCESS_1 = False
        SUCCESS_2 = False

        try:
            # TRYING TO DISPLAY DATA FROM THE FILE
            POST_DATA['BIRD_INFO'] = BIRD_DICTIONARY[name]
            SUCCESS_1 = True
        except KeyError:
            QApplication.instance().webPreview.setHtml(u"<h1> <center> No such bird found </center> </h1>", None)

        try:
            user_agent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
            headers = {'User-Agent': user_agent}
            req = Request(link, None, headers)
            response = urlopen(req)
            page = response.read()  # READING THE PAGE SOURCE
            POST_DATA['ORIGINAL_LINK'] = link
            POST_DATA['LARGEST_IMG'] = findLargestImg(page)
            POST_DATA['AUTHOR'] = findAuthor(page)
            response.close()
            SUCCESS_2 = True
        except Exception:
            QApplication.instance().webPreview.setHtml(u"<h1> <center> Could not connect </center> </h1>", None)

        if SUCCESS_1 and SUCCESS_2:
            QApplication.instance().webPreview.setHtml(createPost(POST_DATA), None)

