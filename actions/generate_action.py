# -*- coding: utf-8 -*-
import os
from pickle import dump

from PySide.QtGui import QMessageBox, QIcon
from openpyxl import load_workbook

from dialogs.generate_file_dialog import GenerateDialog
from models.bird import Bird


def generateFile():
    excel_file = GenerateDialog()
    excel_file.exec_()
    if (excel_file.result() is not None):
        generate(excel_file.result())
        alpha = QMessageBox()
        alpha.setWindowIcon(QIcon("resources/window_icon.png"))
        alpha.setWindowTitle("Success")
        alpha.setText("Indexed bird file generated!")
        alpha.addButton(QMessageBox.Ok)
        alpha.exec_()

def generate(excel_file):
    SAVE_NAME = 'bird-index.avian'  # NAME OF THE FILE YOU ARE SAVING TO
    START_ROW = 4  # THIS IS A ROW FROM WHICH THE ORDER NAMES START, IT NEEDS TO BE THE ROW THAT SAYS 'Order'

    COLUMNS = {
        'ORDER': 'B',  # COLUMN LISTING ORDERS
        'FAMILY': 'C',  # COLUMN LISTING FAMILY
        'GENUS': 'E',  # COLUMN LISTING GENUS
        'SPECIES_SCI': 'F',  # COLUMN LISTING SCIENTIFIC NAME
        'SPECIES_ENG': 'I'  # COLUMN LISTING ENGLISH NAME
    }

    BIRDBOOK = load_workbook(excel_file).get_active_sheet()  # LOADING THE EXCEL FILE AND GETTING ITS ACTIVE SHEET
    BIRD_DICTIONARY = {}  # DICTIONARY WITH KEY : ENGLISH NAME, VALUE : BIRD OBJECT

    for current_row in xrange(START_ROW + 1, BIRDBOOK.max_row + 1):  # We are looking for occurences of 'Order'
        cell_value = BIRDBOOK[COLUMNS['ORDER'] + str(current_row)].value
        if cell_value is not None:  # If the cell contaigns data, we have our match
            ORDER = cell_value.strip(u'†')  # Stripping for that annoying character
            for row in xrange(current_row + 1,
                              BIRDBOOK.max_row + 1):  # We proceed to go down in rows to look for other data
                if BIRDBOOK[COLUMNS['ORDER'] + str(
                        row)].value is not None:  # If we wind another occurence of order we stop the search
                    break
                if BIRDBOOK[COLUMNS['FAMILY'] + str(
                        row)].value is not None:  # We search for non empty value of 'Family'
                    FAMILY = BIRDBOOK[COLUMNS['FAMILY'] + str(row)].value.strip(u'†')
                if BIRDBOOK[COLUMNS['GENUS'] + str(row)].value is not None:  # We search for non empty value of 'Genus'
                    GENUS = BIRDBOOK[COLUMNS['GENUS'] + str(row)].value.strip(u'†')
                if BIRDBOOK[COLUMNS['SPECIES_SCI'] + str(
                        row)].value is not None:  # We search for non empty value of 'Scientific Name'
                    SPECIES_SCI = GENUS + " " + BIRDBOOK[COLUMNS['SPECIES_SCI'] + str(row)].value.strip(u'†')
                if BIRDBOOK[COLUMNS['SPECIES_ENG'] + str(
                        row)].value is not None:  # We search for non empty value of 'English name'
                    SPECIES_ENG = BIRDBOOK[COLUMNS['SPECIES_ENG'] + str(row)].value.strip(u'†')
                    BIRD_DICTIONARY[SPECIES_ENG.encode('utf-8')] = (Bird(ORDER, FAMILY, GENUS, SPECIES_SCI,
                                                                         SPECIES_ENG))  # If found, we create an object and store it in the dictionary

    with open('data/' + SAVE_NAME, "wb") as file:
        dump(BIRD_DICTIONARY, file)  # We finish creating our own file made out of the dictionary
        file.close()