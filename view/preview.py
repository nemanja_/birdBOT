from PySide.QtCore import QUrl
from PySide.QtWebKit import QWebView


class PostPreview(QWebView):

    def __init__(self, path):
        super(PostPreview, self).__init__()
        self.setWindowTitle(path)
        self.load(QUrl.fromLocalFile(path))
