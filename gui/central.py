import os

from PySide.QtGui import QWidget, QApplication, QPushButton, QHBoxLayout, QVBoxLayout, QLineEdit, QTableWidget, \
    QCompleter, QStringListModel

from actions.search_action import bird_search
from view.preview import PostPreview


class Central(QWidget):

    def __init__(self):
        super(Central, self).__init__()
        self.main_layout = QVBoxLayout()
        self.edit_layout = QHBoxLayout()

        self.loadPostPreview(self.main_layout)

        self.loadNameEdit(self.edit_layout)
        self.loadLinkEdit(self.edit_layout)
        self.main_layout.addLayout(self.edit_layout)

        self.loadSearchButton(self.main_layout)
        self.setLayout(self.main_layout)

    def loadNameEdit(self, layout):
        self.name_edit = QLineEdit()
        completer = QCompleter()
        model = QStringListModel()
        model.setStringList(QApplication.instance().bird_index.keys())
        completer.setModel(model)
        self.name_edit.setCompleter(completer)
        self.name_edit.setPlaceholderText("Bird name >> ")
        layout.addWidget(self.name_edit)

    def loadLinkEdit(self, layout):
        self.link_edit = QLineEdit()
        self.link_edit.setPlaceholderText("Link >> ")
        layout.addWidget(self.link_edit)

    def loadSearchButton(self, layout):
        self.search_btn = QPushButton("Go")
        self.search_btn.clicked.connect(self.displaySearch)
        layout.addWidget(self.search_btn)

    def displaySearch(self):
        bird_search(self.name_edit.text(), self.link_edit.text())

    def loadPostPreview(self, layout):
        QApplication.instance().webPreview = PostPreview(os.getcwd() + "\\resources\\home.html")
        layout.addWidget(QApplication.instance().webPreview)