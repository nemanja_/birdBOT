import os

from PySide.QtGui import QMainWindow, QApplication, QLabel, QMenuBar, QAction, QIcon

from actions.generate_action import generateFile
from gui.central import Central


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle(">> BirdBot")
        self.setWindowIcon(QIcon("resources/window_icon.png"))
        self.centralize()
        self.initCentral()
        self.initFileMenu()

    def centralize(self):
        self.setGeometry(0, 0, 800, 600)
        widgetBox = self.frameGeometry()
        centerPoint = QApplication.desktop().availableGeometry().center()
        widgetBox.moveCenter(centerPoint)
        self.move(widgetBox.topLeft())

    def initCentral(self):
        self.setCentralWidget(Central())

    def initFileMenu(self):
        self.menu = QMenuBar()
        fileMenu = self.menu.addMenu("File")

        generateAction = QAction(QIcon('window_icon.png'), 'Generate new .avian file', self)
        fileMenu.addAction(generateAction)
        generateAction.triggered.connect(generateFile)

        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        fileMenu.addAction(exitAction)
        exitAction.triggered.connect(self.close)
        exitAction.setShortcut('Ctrl+Q')

        self.setMenuBar(self.menu)

